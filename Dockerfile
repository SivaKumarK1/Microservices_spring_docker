FROM maven:3.8.4-jdk-11
#VOLUME /tmp
EXPOSE 8080
WORKDIR /Microservices_spring_docker
COPY . /Microservices_spring_docker/.
CMD ["mvn", "clean", "install"]
ENTRYPOINT ["java","-jar","/Microservices_spring_docker/target/Microservices-0.0.1-SNAPSHOT.jar"]
