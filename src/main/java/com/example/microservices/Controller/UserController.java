package com.example.microservices.Controller;

import com.example.microservices.Repo.UserRepo;
import com.example.microservices.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserRepo userRepo;

    @PostMapping("/user")
    private User saveUser(@RequestBody User user) {
        userRepo.save(user);
        return user;
    }

    @GetMapping("/user")
    private List<User> getUser() {
        return userRepo.findAll();
    }
}
