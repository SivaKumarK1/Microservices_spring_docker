package com.example.microservices.constants;

public enum Gender {

    MALE,
    FEMALE
}
